{{/* Expand the name of the chart. */}}
{{- define "moderation-tool.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/* Create a default fully qualified app name. truncated to 63 chars for k8s DNS, dont duplicate with chart name */}}
{{- define "moderation-tool.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}


{{/* auth Resource Names */}}

{{- define "moderation-tool.service" -}}
{{- (printf "%s" (include "moderation-tool.fullname" .)) }}
{{- end }}
{{- define "moderation-tool.serviceWebsite" -}}
{{- (printf "%s-website" (include "moderation-tool.fullname" .)) }}
{{- end }}

{{- define "moderation-tool.deployment" -}}
{{- (printf "%s" (include "moderation-tool.fullname" .)) }}
{{- end }}
{{- define "moderation-tool.deploymentWebsite" -}}
{{- (printf "%s-website" (include "moderation-tool.fullname" .)) }}
{{- end }}

{{- define "moderation-tool.deploymentKind" -}}
{{- if .Values.persistence.enabled -}}StatefulSet{{- else -}}Deployment{{- end }}
{{- end }}

{{- define "moderation-tool.volumeClaim" -}}
{{- (printf "%s-data" (include "moderation-tool.fullname" .)) }}
{{- end }}

{{- define "moderation-tool.serviceAccount" -}}
{{- (printf "%s-serviceaccount" (include "moderation-tool.fullname" .)) }}
{{- end }}

{{- define "moderation-tool.configMapSettings" -}}
{{- (printf "%s-settings" (include "moderation-tool.fullname" .)) }}
{{- end }}

{{- define "moderation-tool.secret" -}}
{{- (printf "%s-secret" (include "moderation-tool.fullname" .)) }}
{{- end }}

{{- define "moderation-tool.ingress" -}}
{{- (printf "%s-ingress" (include "moderation-tool.fullname" .)) }}
{{- end }}
{{- define "moderation-tool.ingressWebsite" -}}
{{- (printf "%s-ingress-website" (include "moderation-tool.fullname" .)) }}
{{- end }}

{{- define "moderation-tool.ingressSecret" -}}
{{- (printf "%s-ingresssecret" (include "moderation-tool.fullname" .)) }}
{{- end }}
{{- define "moderation-tool.ingressSecretWebsite" -}}
{{- (printf "%s-ingresssecret-website" (include "moderation-tool.fullname" .)) }}
{{- end }}

{{- define "moderation-tool.serviceMonitor" -}}
{{- (printf "%s-servicemonitor" (include "moderation-tool.fullname" .)) }}
{{- end }}

{{/* Common labels */}}
{{- define "moderation-tool.labels" -}}
{{ include "moderation-tool.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/* Selector labels */}}
{{- define "moderation-tool.selectorLabels" -}}
app.kubernetes.io/name: {{ include "moderation-tool.deployment" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/* Common labels */}}
{{- define "moderation-tool.labelsWebsite" -}}
{{ include "moderation-tool.selectorLabelsWebsite" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/* Selector labels */}}
{{- define "moderation-tool.selectorLabelsWebsite" -}}
app.kubernetes.io/name: {{ include "moderation-tool.deploymentWebsite" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}