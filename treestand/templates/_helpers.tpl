{{/* Expand the name of the chart. */}}
{{- define "treestand.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/* Create a default fully qualified app name. truncated to 63 chars for k8s DNS, dont duplicate with chart name */}}
{{- define "treestand.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}


{{/* treestand Resource Names */}}

{{- define "treestand.deployment" -}}
{{- (printf "%s" (include "treestand.fullname" .)) }}
{{- end }}

{{- define "treestand.deploymentKind" -}}
Deployment
{{- end }}

{{- define "treestand.serviceAccount" -}}
{{- (printf "%s-serviceaccount" (include "treestand.fullname" .)) }}
{{- end }}

{{- define "treestand.role" -}}
{{- (printf "%s-deployment-updater" (include "treestand.fullname" .)) }}
{{- end }}

{{- define "treestand.roleBinding" -}}
{{- (printf "%s-rolebinding" (include "treestand.fullname" .)) }}
{{- end }}

{{- define "treestand.configmap" -}}
{{- (printf "%s-configmap" (include "treestand.fullname" .)) }}
{{- end }}

{{- define "treestand.configMapGrafana" -}}
{{- (printf "%s-dashboards" (include "treestand.fullname" .)) }}
{{- end }}

{{- define "treestand.secret" -}}
{{- (printf "%s-secret" (include "treestand.fullname" .)) }}
{{- end }}

{{- define "treestand.podMonitor" -}}
{{- (printf "%s-podmonitor" (include "treestand.fullname" .)) }}
{{- end }}

{{/* Common labels */}}
{{- define "treestand.labels" -}}
{{ include "treestand.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/* Selector labels */}}
{{- define "treestand.selectorLabels" -}}
app.kubernetes.io/name: {{ include "treestand.deployment" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}