{{/* Expand the name of the chart. */}}
{{- define "socat-relay.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/* Create a default fully qualified app name. truncated to 63 chars for k8s DNS, dont duplicate with chart name */}}
{{- define "socat-relay.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}


{{/* socat-relay Resource Names */}}

{{- define "socat-relay.service" -}}
{{- (printf "%s" (include "socat-relay.fullname" .)) }}
{{- end }}

{{- define "socat-relay.deployment" -}}
{{- (printf "%s" (include "socat-relay.fullname" .)) }}
{{- end }}

{{- define "socat-relay.deploymentKind" -}}
Deployment
{{- end }}

{{- define "socat-relay.serviceAccount" -}}
{{- (printf "%s-serviceaccount" (include "socat-relay.fullname" .)) }}
{{- end }}

{{- define "socat-relay.secret" -}}
{{- (printf "%s-secret" (include "socat-relay.fullname" .)) }}
{{- end }}

{{/* Common labels */}}
{{- define "socat-relay.labels" -}}
{{ include "socat-relay.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/* Selector labels */}}
{{- define "socat-relay.selectorLabels" -}}
app.kubernetes.io/name: {{ include "socat-relay.deployment" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}