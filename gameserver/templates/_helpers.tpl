{{/* Expand the name of the chart. */}}
{{- define "gameserver.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/* Create a default fully qualified app name. truncated to 63 chars for k8s DNS, dont duplicate with chart name */}}
{{- define "gameserver.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}


{{/* gameserver Resource Names */}}

{{- define "gameserver.service" -}}
{{- (printf "%s" (include "gameserver.fullname" .)) }}
{{- end }}

{{- define "gameserver.deployment" -}}
{{- (printf "%s" (include "gameserver.fullname" .)) }}
{{- end }}

{{- define "gameserver.deploymentKind" -}}
{{- if .Values.persistence.enabled -}}StatefulSet{{- else -}}Deployment{{- end }}
{{- end }}

{{- define "gameserver.volumeClaim" -}}
{{- (printf "%s-data" (include "gameserver.fullname" .)) }}
{{- end }}

{{- define "gameserver.serviceAccount" -}}
{{- (printf "%s-serviceaccount" (include "gameserver.fullname" .)) }}
{{- end }}

{{- define "gameserver.configMapSettings" -}}
{{- (printf "%s-settings" (include "gameserver.fullname" .)) }}
{{- end }}

{{- define "gameserver.configMapGrafana" -}}
{{- (printf "%s-dashboards" (include "gameserver.fullname" .)) }}
{{- end }}

{{- define "gameserver.secret" -}}
{{- (printf "%s-secret" (include "gameserver.fullname" .)) }}
{{- end }}

{{- define "gameserver.ingressSecret" -}}
{{- (printf "%s-ingresssecret" (include "gameserver.fullname" .)) }}
{{- end }}

{{- define "gameserver.ingress" -}}
{{- (printf "%s-ingress" (include "gameserver.fullname" .)) }}
{{- end }}

{{- define "gameserver.podMonitor" -}}
{{- (printf "%s-podmonitor" (include "gameserver.fullname" .)) }}
{{- end }}

{{/* Common labels */}}
{{- define "gameserver.labels" -}}
{{ include "gameserver.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/* Selector labels */}}
{{- define "gameserver.selectorLabels" -}}
app.kubernetes.io/name: {{ include "gameserver.deployment" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}